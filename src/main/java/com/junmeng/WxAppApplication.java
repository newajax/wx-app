package com.junmeng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class WxAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(WxAppApplication.class, args);
	}
}
