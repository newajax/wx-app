package com.junmeng.service.impl;

import com.junmeng.entity.Area;
import com.junmeng.repository.AreaMapper;
import com.junmeng.service.IAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 名称: AreaServiceImpl.java <br>
 * 描述:<br>
 * 类型: JAVA<br>
 * 最近修改时间:2017/7/11 10:17<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2017/7/11 10:17
 */
@Service
@Transactional
public class AreaServiceImpl implements IAreaService{
    @Autowired
    private AreaMapper areaMapper;

    @Override
    public Area getById(Integer id) {
        return areaMapper.selectByPrimaryKey(id);
    }
}
