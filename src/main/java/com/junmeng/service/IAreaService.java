package com.junmeng.service;

import com.junmeng.entity.Area;

/**
 * 名称: IAreaService.java <br>
 * 描述:<br>
 * 类型: JAVA<br>
 * 最近修改时间:2017/7/11 10:16<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2017/7/11 10:16
 */
public interface IAreaService {

	Area getById(Integer id);
}
