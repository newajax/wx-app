package com.junmeng.controller;

import com.junmeng.entity.Area;
import com.junmeng.service.IAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import sun.dc.pr.PRError;

/**
 * 名称: DemoController.java <br>
 * 描述:<br>
 * 类型: JAVA<br>
 * 最近修改时间:2017/7/11 10:19<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2017/7/11 10:19
 */
@Controller
@RequestMapping(value = "/")
public class DemoController {


    @Autowired
    private IAreaService areaService;

    @RequestMapping(value = "/get")
    @ResponseBody
    public Area get(){
        return areaService.getById(1);
    }

}
