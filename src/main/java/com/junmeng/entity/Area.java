package com.junmeng.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
public class Area implements Serializable{
    private static final long serialVersionUID = 3576034419305130111L;
    private Integer areaId;

    private Integer parentId;

    private String name;

    private String fullName;

    private String treeCode;

    private String pinyinName;

    private Integer type;

    private Integer recordStatus;


}